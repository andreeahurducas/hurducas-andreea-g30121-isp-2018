package andreea.hurducas.lab5.ex2;

public class RealImage implements Image {
    private String fileName;

    public RealImage(String fileName) {
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    @Override
    public void display() {
        System.out.println("Display real image " + fileName);
    }

    private void loadFromDisk(String fileName) {
        System.out.println("Loading real image " + fileName);
    }
}
