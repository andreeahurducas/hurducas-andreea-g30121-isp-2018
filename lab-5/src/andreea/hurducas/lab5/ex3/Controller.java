package andreea.hurducas.lab5.ex3;

import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    private static int count = 0;


    public void control() {
        TemperatureSensor tss = new TemperatureSensor();
        LightSensor lss = new LightSensor();
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {


            public void run() {
                count++;
                if (count <= 20) {
                    System.out.println(count + " )Temparature value: " + tss.readValue() + " and light value " + lss.readValue());
                    System.out.println(count + " values have been read");
                } else {
                    System.out.println("stop");
                    timer.cancel();
                }

            }

        };
        timer.schedule(task, 0, 1000);

    }
}

