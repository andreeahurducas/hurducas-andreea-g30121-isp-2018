package andreea.hurducas.lab5.ex4;

import andreea.hurducas.lab5.ex3.LightSensor;
import andreea.hurducas.lab5.ex3.TemperatureSensor;

public class Controller {
    private Controller() {

    }

    ;
    private static Controller Controller;


    public static void control() throws InterruptedException {


        TemperatureSensor ts = new TemperatureSensor();
        LightSensor ls = new LightSensor();


        int sec = 1;
        while (sec <= 20) {
            System.out.println("Temp: " + ts.readValue());
            System.out.println("Light: " + ls.readValue());
            System.out.println("Sec: " + sec);
            sec++;
            Thread.sleep(1000);
        }


    }


    public static void main(String[] args) throws InterruptedException {

        Controller.control();

    }
}
