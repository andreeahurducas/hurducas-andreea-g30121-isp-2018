package andreea.hurducas.lab5.ex1;

public class Rectangle extends Shape {
    protected double width;
    protected double length;

    public Rectangle() {
    }

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length, String color, boolean filled) {
        super(color, filled);
        this.length = length;
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        // TODO Auto-generated method stub
        return this.length * this.width;
    }

    @Override
    public double getPerimeter() {
        // TODO Auto-generated method stub
        return 2 * this.width + 2 * this.length;
    }

    @Override
    public String toString() {
        return "Rectangle [width=" + this.width + ", length=" + this.length + " which is a subclass of "
                + super.toString() + "]";
    }

}
