package andreea.hurducas.lab5.ex1;

public class TestShape {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[3];
        shapes[0] = new Circle(5, "red", true);
        shapes[1] = new Rectangle(3, 4, "blue", false);
        shapes[2] = new Square(5, "black", true);


        for (Shape s : shapes) {

            System.out.println(s.toString());
            System.out.println("The area is " + s.getArea());
            System.out.println("The perimeter is " + s.getPerimeter());

        }

    }
}
