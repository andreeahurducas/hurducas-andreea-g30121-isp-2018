package andreea.hurducas.lab4.ex4;

import andreea.hurducas.lab4.ex2.Author;

public class TestBook {
    public static void main(String args[]) {
        Author a1 = new Author("J.K Rowling", "jkr@yahoo.com", 'm');
        Author a2 = new Author("George R.R Martin", "georgerm@gmail.com", 'm');
        Author a3 = new Author("J R.R Tolkien", "jrrtolkien@gmail.com", 'm');
        Author[] authors = new Author[3];
        authors[0] = a1;
        authors[1] = a2;
        authors[2] = a3;
        Book b1 = new Book("Harry Potter", authors, 25, 25);

        authors = new Author[2];
        authors[0] = a1;
        authors[1] = a3;
        Book b2 = new Book("Game of Thrones", authors, 40, 15);

        authors = new Author[2];
        authors[0] = a1;
        authors[1] = a3;
        Book b3 = new Book("The Hobbit", authors, 35, 45);


        System.out.println("Book Name : " + b1.getName());
        System.out.println("Authors: ");
        b1.printAuthors();
        System.out.println();
        System.out.println("Price : " + b1.getPrice());
        b1.setPrice(75);
        System.out.println("New Price : " + b1.getPrice());
        System.out.println("Quantity : " + b1.getQtyInStock());
        b1.setQtyInStock(80);
        System.out.println("New Quantity : " + b1.getQtyInStock());
        System.out.println(b1.toString());

        System.out.println("\n");

        System.out.println("Book Name : " + b2.getName());
        System.out.println("Authors: ");
        b2.printAuthors();
        System.out.println();
        System.out.println("Price : " + b2.getPrice());
        b2.setPrice(45);
        System.out.println("New Price : " + b2.getPrice());
        System.out.println("Quantity : " + b2.getQtyInStock());
        b2.setQtyInStock(4);
        System.out.println("New Quantity : " + b2.getQtyInStock());
        System.out.println(b2.toString());

        System.out.println("\n");

        System.out.println("Book Name : " + b3.getName());
        System.out.println("Authors: ");
        b3.printAuthors();
        System.out.println();
        System.out.println("Price : " + b3.getPrice());
        b3.setPrice(100);
        System.out.println("New Price : " + b3.getPrice());
        System.out.println("Quantity : " + b3.getQtyInStock());
        b3.setQtyInStock(60);
        System.out.println("New Quantity : " + b3.getQtyInStock());
        System.out.println(b3.toString());
    }
}

