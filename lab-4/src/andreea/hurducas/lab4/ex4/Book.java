package andreea.hurducas.lab4.ex4;

import andreea.hurducas.lab4.ex2.Author;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock = 0;

    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public String toString() {
        String s = this.name + " by " + authors.length + " authors";
        return s;
    }

    public void printAuthors() {
        for (int i = 0; i < authors.length; i++) {
            System.out.println(this.authors[i].toString() + ", ");
        }
    }
}
