package andreea.hurducas.lab4.ex6;

public class Test {

    public static void main(String[] args) {
        Shape s1 = new Shape();
        Shape s2 = new Shape("blue", false);
        System.out.println("s1 :" + s1.toString());
        s1.setColor("black");
        s1.setFilled(false);
        System.out.println("new s1 :" + s1.toString());
        System.out.println(s2.toString());

        System.out.println("\n");
        Circle c1 = new Circle();
        Circle c2 = new Circle(4, "pink", false);
        System.out.println(c1.toString());
        System.out.println(c2.toString());
        System.out.println("The area of c1 is " + c1.getArea());
        System.out.println("The perimeter of c2 is " + c2.getPerimeter());

        System.out.println("\n");
        Rectangle r1 = new Rectangle();
        Rectangle r2 = new Rectangle(4, 5);
        Rectangle r3 = new Rectangle("blue", true, 3, 4);
        System.out.println(r1.toString());
        System.out.println(r2.toString());
        System.out.println(r3.toString());
        System.out.println("The area of r3 is " + r3.getArea());
        System.out.println("The perimeter of r2 is " + r2.getPerimeter());

        System.out.println("\n");
        Square sq1 = new Square();
        Square sq2 = new Square(4);
        Square sq3 = new Square("brown", false, 3);
        System.out.println(sq1.toString());
        System.out.println(sq2.toString());
        System.out.println(sq3.toString());
        System.out.println(sq2.getLength() + " , " + sq2.getWidth());
        System.out.println(sq1.getSide());
        System.out.println(sq3.getArea());
    }
}
