package andreea.hurducas.lab4.ex6;

public class Shape {
    private String color = "red";
    private boolean filled=true;

    public Shape() {
        this.color = "green";
        this.filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public boolean isFilled() {
        return filled;
    }

    @Override
    public String toString() {
        String s;
        if (this.filled)
            s = "filled";
        else {
            s = "not filled";
        }
        return "Shape{" + "color= " + color + ",is " + s + "}";
    }
}
