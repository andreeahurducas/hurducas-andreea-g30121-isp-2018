package andreea.hurducas.lab4.ex3;

import andreea.hurducas.lab4.ex2.Author;

public class TestBook {
    public static void main(String args[]) {
        Author a1 = new Author("J.K Rowling", "jkr@yahoo.com", 'm');
        Author a2 = new Author("George R.R Martin", "georgerm@gmail.com", 'm');
        Author a3 = new Author("J R.R Tolkien", "jrrtolkien@gmail.com", 'm');
        Book b1 = new Book("Harry Potter", a1, 25);
        Book b2 = new Book("The Hobbit", a2, 35);
        Book b3 = new Book("Game of Thrones", a3, 40);
        System.out.println("The title of the book is " + b1.getName());
        System.out.println("The price of " + b2.getName() + " is " + b2.getPrice());
        System.out.println("The author of " + b3.getName() + " is " + b3.getAuthor());
        b1.setPrice(45);
        b2.setQtyInStock(235);
        System.out.println("The new price of " + b1.getName() + " is " + b1.getPrice());
        System.out.println("There are  " + b2.getQtyInStock()+" " + b2.getName() + " books in stock");
        System.out.println(b1.toString());
    }
}
