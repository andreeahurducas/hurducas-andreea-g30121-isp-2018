package andreea.hurducas.lab4.ex2;

public class TestAuthor {
    public static void main(String args[]) {
        Author a1 = new Author("J.K Rowling", "jkr@yahoo.com", 'm');
        Author a2 = new Author("George R.R Martin", "georgerm@gmail.com", 'm');
        Author a3 = new Author("J R.R Tolkien", "jrrtolkien@gmail.com", 'm');
        System.out.println("The name of the first person is " + a1.getName());
        System.out.println("The email address is " + a1.getEmail());
        System.out.println("The gender is  " + a1.getGender() + " where f means female and m means male");
        System.out.println(a2.getName());
        System.out.println(a2.getEmail());
        System.out.println(a2.getGender());
        System.out.println(a3.getName());
        System.out.println(a3.getEmail());
        System.out.println(a3.getGender());
        a1.toString();
        a1.setEmail("andreeah@yahoo.com");
        System.out.println(a1.getEmail());
    }
}
