package andreea.hurducas.lab4.ex5;

public class TestCylinder {
    public static void main(String[] args) {
        Cylinder c1 = new Cylinder(3);
        Cylinder c2 = new Cylinder(5, 4);
        System.out.println("The area of c1 = " + c1.getArea());
        System.out.println("The area of c2= " + c2.getArea());
        System.out.println("The volume of c1= " + c1.getVolume());
        System.out.println("The volume of c2= " + c2.getVolume());
    }
}
