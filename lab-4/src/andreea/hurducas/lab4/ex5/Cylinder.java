package andreea.hurducas.lab4.ex5;

import andreea.hurducas.lab4.ex1.Circle;

public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder() {
    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public double getArea() {
        return 2 * Math.PI * super.getRadius() * this.height + 2 * super.getArea();

    }

    public double getVolume() {
        return super.getArea() * this.height;
    }
}
