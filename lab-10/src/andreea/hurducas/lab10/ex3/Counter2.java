package andreea.hurducas.lab10.ex3;

public class Counter2 extends Thread {
    Counter2(String name) {
        super(name);
    }

    public void run(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println(getName() + " i = " + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }


    public static void main(String[] args) {
        Counter2 c1 = new Counter2("counter1");
        Counter2 c2 = new Counter2("counter2");
        c1.run(101);
        c2.run(201);


    }
}
