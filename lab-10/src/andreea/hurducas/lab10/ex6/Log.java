package andreea.hurducas.lab10.ex6;

import java.util.Date;

public class Log {
    private String Class;
    private Date time;

    public Log(String Class) {
        this.Class = Class;
    }

    @SuppressWarnings("deprecation")
    private String getTime() {
        time = new Date();
        return String.format("%02d:%02d:%02d", time.getHours(), time.getMinutes(), time.getSeconds());
    }

    public void info(String m) {
        System.out.printf("[%s][INFO][%s.class]%s\n", getTime() ,Class, m);
    }

    public void debug(String m) {
        System.out.printf("[%s][DEBUG][%s.class]%s\n", getTime() ,Class, m);
    }
}
