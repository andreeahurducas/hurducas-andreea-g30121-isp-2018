package andreea.hurducas.lab2.ex3;

import java.util.Scanner;

public class PrimeNumbers  {
    public static void main(String args[]) {
        int n1, n2, s3, flag = 0, i, j;
        Scanner s = new Scanner(System.in);
        System.out.println("Dati numarul mai mic :");
        n1 = s.nextInt();
        System.out.println("Dati numarul mai mare :");
        n2 = s.nextInt();
        System.out.println("Numerele prime intre cele 2 limite sunt :");
        for (i = n1; i <= n2; i++) {
            for (j = 2; j < i; j++) {
                if (i % j == 0) {
                    flag = 0;
                    break;
                } else {
                    flag = 1;
                }
            }
            if (flag == 1) {
                System.out.println(i);
            }
        }
    }
}

