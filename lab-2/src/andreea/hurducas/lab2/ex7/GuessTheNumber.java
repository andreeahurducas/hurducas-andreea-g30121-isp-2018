package andreea.hurducas.lab2.ex7;

import java.util.*;
import java.util.Scanner;

public class GuessTheNumber {
    public static void main(String[] args) {
        Random r = new Random();
        int a = r.nextInt(10);
        int count = 1;


        System.out.print("Guess the number! You have 3 chances!");
        System.out.println();

        Scanner in = new Scanner(System.in);

        while (count <= 3) {
            System.out.println();
            int n1;
            System.out.print("Chance =  " + count);
            System.out.println();
            System.out.print("Introduce the first number =  ");
            System.out.println();
            n1 = in.nextInt();
            if (n1 == a) {
                System.out.print("You guessed the number");
                System.out.println();
                break;
            }
            if (n1 < a)
                System.out.print("Wrong answer, your number is too low");
            if (n1 > a)
                System.out.print("Wrong answer, your number is too high");

            count = count + 1;

            System.out.println();

            int n2;
            System.out.print("Chance =  " + count);
            System.out.println();
            System.out.print("Introduce the second number =  ");
            System.out.println();
            n2 = in.nextInt();
            if (n2 == a) {
                System.out.print("You guessed the number");
                break;
            }
            if (n2 < a)
                System.out.print("Wrong answer, your number is too low");
            if (n2 > a)
                System.out.print("Wrong answer, your number is too high");

            count = count + 1;

            System.out.println();
            int n3;
            System.out.print("Chance =  " + count);
            System.out.println();
            System.out.print("Introduce the third number =  ");
            System.out.println();
            n3 = in.nextInt();
            if (n3 == a) {
                System.out.print("You guessed the number");
                break;
            }
            if (n3 < a) {
                System.out.print("Wrong answer, your number is too low. You lost!");
                System.out.println();
                System.out.print("The number you should have guessed is " + a);
            }
            if (n3 > a) {
                System.out.print("Wrong answer, your number is too high. You lost!");
                System.out.println();
                System.out.print("The number you should have guessed " + a);
            }
            count = count + 1;
        }
    }
}

