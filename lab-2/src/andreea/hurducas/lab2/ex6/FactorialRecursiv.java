package andreea.hurducas.lab2.ex6;

import java.util.Scanner;

public class FactorialRecursiv {
    public static int factorial(int n) {
        if (n > 1)
            return n * factorial(n - 1);
        else
            return 1;
    }

    public static void main(String[] args) {
        int n, fact = 1;
        Scanner in = new Scanner(System.in);
        System.out.print("n =  ");
        n = in.nextInt();
        if (n < 0) {
            System.out.print("Nu se poate calcula factorialul");
            System.out.println();
        } else if (n == 0) {
            System.out.print("Factorialul este 1");
            System.out.println();
        } else if (n == 1) {
            System.out.print("Factorialul este 1");
            System.out.println();
        } else if (n > 1) {
            fact = factorial(n);
            System.out.print("Factorialul  este : " + fact);
            System.out.println();
        }
    }
}

