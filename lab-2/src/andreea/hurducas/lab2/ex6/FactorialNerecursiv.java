package andreea.hurducas.lab2.ex6;

import java.util.Scanner;

public class FactorialNerecursiv {

    public static void main(String[] args) {
        int n, fact = 1;
        Scanner in = new Scanner(System.in);
        System.out.print("n =  ");
        n = in.nextInt();
        if (n < 0) {
            System.out.println("nu putem calcula factorialul");

        } else if (n == 0) {
            System.out.println("Factorialul este 1");
        } else if (n == 1) {
            System.out.println("Factorialul este 1");
        } else if (n >= 2) {

            int i;
            for (i = 1; i <= n; i++)
                fact = fact * i;


            System.out.println("Factorialul  este : " + fact);
            System.out.println();
        }
    }
}
