package andreea.hurducas.lab2.ex4;

import java.util.Scanner;

public class MaximVector {
    public static void main(String args[]) {
        Scanner s = new Scanner(System.in);
        System.out.println("Dati lungimea vectorului ");
        int n = s.nextInt(), i = 0, max = 0;
        int[] v = new int[n];
        for (i = 0; i < n; i++) {
            System.out.println("v[" + i + "]=");
            v[i] = s.nextInt();
        }
        for (i = 0; i < n; i++) {
            if (v[i] > max) {
                max = v[i];
            }
        }
        System.out.println("Maximul este " + max);
    }
}



