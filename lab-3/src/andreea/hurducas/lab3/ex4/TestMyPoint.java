package andreea.hurducas.lab3.ex4;

public class TestMyPoint {
    public static void main(String args[]) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint();
        MyPoint p3 = new MyPoint(3, 4);
        MyPoint p4 = new MyPoint(13, 3);
        MyPoint p5 = new MyPoint(17, 26);
        System.out.println(p1.toString());
        System.out.println(p2.toString());
        System.out.println(p3.toString());
        System.out.println(p4.toString());
        System.out.println(p5.toString());
        p1.setX(4);
        p1.setY(9);
        System.out.println(p1.getX());
        System.out.println(p1.getY());
        p1.toString();
        p2.setXY(11, 5);
        System.out.println(p2.toString());
        System.out.println(p3.distance(0, 0));
        System.out.println(p3.distance(p2));
        System.out.println(p3.distance(p4));

    }
}
