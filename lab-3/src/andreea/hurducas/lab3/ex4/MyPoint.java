package andreea.hurducas.lab3.ex4;

public class MyPoint {
    int x;
    int y;

    MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        String c = "(" + Integer.toString(this.x) + ", " + Integer.toString(this.y) + ")";
        return c;
    }

    public double distance(int x, int y) {
        return Math.sqrt((x - this.x) * (x - this.x) + (y - this.y) * (y - this.y));
    }

    public double distance(MyPoint m) {
        return Math.sqrt((m.x - this.x) * (m.x - this.x) + (m.y - this.y) * (m.y - this.y));
    }
}
