package andreea.hurducas.lab3.ex5;

public class Flower {

    int petal;
    static int var = 0;

    Flower() {
        System.out.println("Flower has been created!");
        var++;
    }

    public static int ob() {
        return var;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for (int i = 0; i < 5; i++) {
            Flower f = new Flower();
            garden[i] = f;
        }
        System.out.println("There have been created " + ob() + " flowers");
    }
}

