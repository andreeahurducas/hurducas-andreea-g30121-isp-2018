package andreea.hurducas.lab3.ex1;

public class Sensor {
    int value;

    public Sensor() {
        this.value = -1;
    }

    public void change(int k) {
        this.value = k;
    }

    public String toString() {
        String value = Integer.toString(this.value);
        return value;
    }
}
