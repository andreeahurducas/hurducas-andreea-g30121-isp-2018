package andreea.hurducas.lab3.ex1;

public class TestSensor {
    public static void main(String[] args) {
        Sensor s1 = new Sensor();
        System.out.println("The value of the sensor s1 is " + s1.toString());
        Sensor s2 = new Sensor();
        System.out.println("The value of the sensor s2 is " + s2.toString());
        s2.change(22);
        System.out.println("The value of the sensor s2 after the change has been made is " + s2.toString());
    }
}
