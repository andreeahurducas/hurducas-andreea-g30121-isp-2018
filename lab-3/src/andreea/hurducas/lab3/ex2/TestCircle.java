package andreea.hurducas.lab3.ex2;

public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        System.out.println("The first circle has the length of the radius " + c1.getRadius());
        Circle c2 = new Circle(2);
        System.out.println("The second circle has the length of the radius " + c2.getRadius() + " the color " + c2.getColor());
        Circle c3 = new Circle(3, "black");
        System.out.println(
                "The second circle has the length of the radius  " + c3.getRadius() + " ,its color is " + c3.getColor() + " and its area is " + c3.getArea());
    }
}
