package andreea.hurducas.lab9.ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CountApp implements ActionListener {
    JButton inc = new JButton("CLICK");
    JFrame jf = new JFrame();
    JTextField displaytf=new JTextField(10);
    int i = 0;


    public CountApp() {

        displaytf.setText("" + i);
        jf.setTitle("Counter");
        jf.setVisible(true);
        jf.setSize(200, 100);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setResizable(true);
        jf.setLayout(new FlowLayout());
        jf.add(displaytf);
        jf.add(inc);
        inc.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == inc) {
            i++;
            displaytf.setText("" + i);
        }

    }

    public static void main(String args[]) {
        new CountApp();
    }
}
