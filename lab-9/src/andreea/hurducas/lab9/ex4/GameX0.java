package andreea.hurducas.lab9.ex4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameX0 extends JFrame {
    JButton[] b;
    int ind;


    GameX0() {
        setTitle("GameX0");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 360);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 80;

        b = new JButton[9];
        b[0] = new JButton();
        b[0].setBounds(10, 10, width, height);
        b[0].addActionListener(new TratareButon());
        b[1] = new JButton();
        b[1].setBounds(10, 110, width, height);
        b[1].addActionListener(new TratareButon());
        b[2] = new JButton();
        b[2].setBounds(10, 210, width, height);
        b[2].addActionListener(new TratareButon());
        b[3] = new JButton();
        b[3].setBounds(110, 10, width, height);
        b[3].addActionListener(new TratareButon());
        b[4] = new JButton();
        b[4].setBounds(110, 110, width, height);
        b[4].addActionListener(new TratareButon());
        b[5] = new JButton();
        b[5].setBounds(110, 210, width, height);
        b[5].addActionListener(new TratareButon());
        b[6] = new JButton();
        b[6].setBounds(210, 10, width, height);
        b[6].addActionListener(new TratareButon());
        b[7] = new JButton();
        b[7].setBounds(210, 110, width, height);
        b[7].addActionListener(new TratareButon());
        b[8] = new JButton();
        b[8].setBounds(210, 210, width, height);
        b[8].addActionListener(new TratareButon());
        for (int i = 0; i < 9; i++) add(b[i]);
    }
    public void resetButtons()
    {
        for(int i = 0; i < 9; i++)
        {
            b[i].setText("");
        }
    }

    public static void main(String[] args) {
        new GameX0();
    }

    class TratareButon implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JButton buttonClicked = (JButton)e.getSource();
            if(ind%2 == 0)
                buttonClicked.setText("X");
            else
                buttonClicked.setText("O");

            if(checkForWin() == true)
            {
                JOptionPane.showConfirmDialog(null, "You Win!");
                resetButtons();
            }
            ind++;

        }
        public boolean checkForWin()
        {

            if( checkAdjacent(0,1) && checkAdjacent(1,2) ) //no need to put " == true" because the default check is for true
                return true;
            else if( checkAdjacent(3,4) && checkAdjacent(4,5) )
                return true;
            else if ( checkAdjacent(6,7) && checkAdjacent(7,8))
                return true;

                //vertical win check
            else if ( checkAdjacent(0,3) && checkAdjacent(3,6))
                return true;
            else if ( checkAdjacent(1,4) && checkAdjacent(4,7))
                return true;
            else if ( checkAdjacent(2,5) && checkAdjacent(5,8))
                return true;
            else if ( checkAdjacent(0,4) && checkAdjacent(4,8))
                return true;
            else if ( checkAdjacent(2,4) && checkAdjacent(4,6))
                return true;
            else
                return false;

        }
        public boolean checkAdjacent(int j, int i)
        {
            if ( b[j].getText().equals(b[i].getText()) && !b[j].getText().equals("") )
                return true;
            else
                return false;
        }

    }

}
