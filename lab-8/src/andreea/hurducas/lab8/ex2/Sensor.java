package andreea.hurducas.lab8.ex2;

public class Sensor {
    int valoare;

    Sensor(int a){
        valoare = a;
    }

    void afiseaza(){
        System.out.println("Temperatura="+valoare);
    }

    public static void main(String[] args){
        Sensor s1 = new Sensor(25);
        Compresor z1 = new Compresor();
        Controler c1 = new Controler(s1,z1);
        c1.control();

    }
}
