package andreea.hurducas.lab8.ex2;

public class Controler {

    static final int V = 20;

    Sensor s;
    Compresor c;

    Controler(Sensor s, Compresor c) {
        this.s = s;
        this.c = c;
    }

    void control() {
        if (s.valoare > V)
            c.pornesteCompresor();
        else
            c.opresteCompresor();
    }
}
