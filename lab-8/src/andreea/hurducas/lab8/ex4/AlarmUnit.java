package andreea.hurducas.lab8.ex4;

public class AlarmUnit {
    protected String owner;

    public AlarmUnit() {
    }

    public AlarmUnit(String owner) {
        this.owner = owner;
    }

    public void event(Event event) throws Exception {

        if (event.getType().equals(EventType.FIRE) && ((FireEvent)event).isSmoke() == true)
        {
            GSMUnit g = new GSMUnit();
            g.callOwner();
            throw new Exception("Fire in the house");
        }

    }
}
