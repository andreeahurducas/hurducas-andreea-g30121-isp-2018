package andreea.hurducas.lab8.ex4;

import java.util.ArrayList;
import java.util.Scanner;

public class ControlUnit {
    //declaring multiple fireSensors and one tempSensor
    private ArrayList<FireSensor> fireSensors = new ArrayList<FireSensor>();
    private TemperatureSensor temperatureSensor;
    private static String owner;
    private static int desiredTemperature;


    //singleton implementation

    private ControlUnit() {
    }

    ;
    private static ControlUnit controlUnit;

    public static ControlUnit getInstance() {
        if (controlUnit == null) {
            controlUnit = new ControlUnit();
            Scanner scan = new Scanner(System.in);

            System.out.println("Enter house owner: ");
            owner = scan.nextLine();
            System.out.println("Enter desired temperature: ");
            desiredTemperature = scan.nextInt();
        }


        return controlUnit;
    }
}
