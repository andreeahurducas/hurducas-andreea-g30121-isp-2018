package andreea.hurducas.lab8.ex4;

public class CoolingUnit {
    public void cooling(int desiredTemp) {

        System.out.println("Cooling to reach desired temperature: " + desiredTemp);
    }
}
