package andreea.hurducas.lab11.ex1;

import java.util.Observable;

public interface Observer {
    void update(Observable t, Object o);
}
