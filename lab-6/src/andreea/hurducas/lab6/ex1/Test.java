package andreea.hurducas.lab6.ex1;

public class Test {
    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Andreea",1000);
        BankAccount b2 = new BankAccount("Andreea",1000);
        BankAccount b3 = new BankAccount("Victor",1500);
        BankAccount b4 = new BankAccount("Mihai",500);
        if(b1.equals(b2))
            System.out.println(b1+" and "+b2+ " are equals");
        else
            System.out.println(b1+" and "+b2+ " are NOT equals");
        if(b4.equals(b3))
            System.out.println(b4+" and "+b3+ " are equals");
        else
            System.out.println(b4+" and "+b3+ " are NOT equals");

        System.out.println(b1.hashCode());
        System.out.println(b2.hashCode());
    }
}
