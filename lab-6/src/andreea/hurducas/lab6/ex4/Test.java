package andreea.hurducas.lab6.ex4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Test {
    public static void main(String[] args) throws IOException {

        Dictionary dict = new Dictionary();
        char rsp;
        String line;
        String ex;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("~MENU~");
            System.out.println("a - Add word");
            System.out.println("b - Search word");
            System.out.println("c- List the words");
            System.out.println("d - List the definitions");
            System.out.println("e - List the dictionary");
            System.out.println("f - Exit");

            line = fluxIn.readLine();
            rsp = line.charAt(0);

            switch (rsp) {
                case 'a':
                case 'A':
                    System.out.println("Add a word:");
                    line = fluxIn.readLine();
                    if (line.length() > 1) {
                        System.out.println("Add a definition:");
                        ex = fluxIn.readLine();
                        dict.addWord(new Word(line), new Definition(ex));
                    }
                    break;
                case 'b':
                case 'B':
                    System.out.println("Your word:");
                    line = fluxIn.readLine();
                    if (line.length() > 1) {
                        Word x = new Word(line);
                        ex = String.valueOf(dict.getDefinition(x));

                        if (ex == null)
                            System.out.println("The word!");
                        else
                            System.out.println("Definition:" + ex);
                    }
                    break;
                case 'c':
                case 'C':
                    System.out.println("Display:");
                    dict.afisDictionar();
                    break;
                case 'd':
                case 'D':
                    System.out.println("Get all words");
                    dict.getAllWords();
                    break;
                case 'e':
                case 'E':
                    System.out.println("Get all definitions");
                    dict.getAllDefinitions();
                    break;

            }
        } while (rsp != 'f' && rsp != 'F');
        System.out.println("Program ended.");
    }
}
