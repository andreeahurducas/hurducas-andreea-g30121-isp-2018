package andreea.hurducas.lab6.ex3;

public class Test {

    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Stefan",1900);
        bank.addAccount("Matei",7000);
        bank.addAccount("Anca",1900);
        bank.addAccount("Ionut",8000);
        bank.addAccount("Alina",1500);


        System.out.println("Print Accounts by balance");
        bank.printAccounts();

        System.out.println("Print Accounts between limits");
        bank.printAccounts(800,2200);

        System.out.println("Get Account by owner name");
        bank.getAccount("Alina",1500);

        System.out.println("Get All Accounts");
        bank.getAllAccount();

    }
}
