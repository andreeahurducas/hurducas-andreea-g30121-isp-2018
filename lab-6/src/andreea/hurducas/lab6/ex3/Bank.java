package andreea.hurducas.lab6.ex3;

import java.util.TreeSet;

public class Bank {
    TreeSet treeBankAccounts = new TreeSet();

    public void addAccount(String owner, double balance) {
        BankAccount ba = new BankAccount(owner, balance);
        treeBankAccounts.add(ba);
    }

    public void printAccounts() {
        System.out.println(treeBankAccounts);
    }

    public void printAccounts(double minBalance, double maxBalance) {
        System.out.println(treeBankAccounts.subSet(new BankAccount("x", minBalance), new BankAccount("y", maxBalance)));
    }

    public BankAccount getAccount(String owner, double balance) {
        System.out.println(treeBankAccounts.contains(new BankAccount(owner, balance)));
        return null;
    }

    public BankAccount getAllAccount() {
        System.out.println(treeBankAccounts);
        return null;
    }

}
