package andreea.hurducas.lab6.ex3;

public class BankAccount implements Comparable {
    public String owner;
    public double balance;

    public BankAccount(String owner, double balance) {
        this.balance = balance;
        this.owner = owner;
    }

    public void withdraw(double amount) {
        this.balance = this.balance - amount;
    }

    public void deposit(double amount) {
        this.balance = this.balance + amount;
    }

    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount ba = (BankAccount) obj;
            return balance == ba.balance && ba.owner.equals(owner);
        }
        return false;
    }

    public int hashCode() {
        return (int) balance + owner.hashCode();
    }

    public int compareTo(Object o) {
        BankAccount p = (BankAccount) o;
        if (balance > p.balance) return 1;
        if (balance == p.balance) return 0;
        return -1;
    }

    public String toString() {
        return owner + ":" + balance;
    }
}
