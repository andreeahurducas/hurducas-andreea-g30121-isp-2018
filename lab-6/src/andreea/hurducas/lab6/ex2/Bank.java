package andreea.hurducas.lab6.ex2;

import java.util.ArrayList;

public class Bank {
    ArrayList bankAccounts = new ArrayList();

    public void addAccount(String owner, double balance) {
        BankAccount b = new BankAccount(owner, balance);
        bankAccounts.add(b);
    }

    public void printAccounts() {
        int sortat;
        do {
            sortat = 1;
            for (int i = 0; i < bankAccounts.size() - 1; i++) {
                BankAccount b1 = (BankAccount) bankAccounts.get(i);
                BankAccount b2 = (BankAccount) bankAccounts.get(i + 1);
                BankAccount b3 = new BankAccount(" ", 0);
                if (b1.balance > b2.balance) {
                    b3.owner = b1.owner;
                    b3.balance = b1.balance;
                    b1.owner = b2.owner;
                    b1.balance = b2.balance;
                    b2.owner = b3.owner;
                    b2.balance = b3.balance;
                    sortat = 0;
                }
            }
        } while (sortat == 0);

        for (int i = 0; i < bankAccounts.size(); i++) {
            BankAccount ba = (BankAccount) bankAccounts.get(i);
            System.out.println(ba.owner + " " + ba.balance);
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (int i = 0; i < bankAccounts.size(); i++) {
            BankAccount b = (BankAccount) bankAccounts.get(i);
            if (b.balance >= minBalance && b.balance <= maxBalance)
                System.out.println(b.owner + " " + b.balance);
        }
    }

    public BankAccount getAccount(String owner) {
        for (int i = 0; i < bankAccounts.size(); i++) {
            BankAccount b = (BankAccount) bankAccounts.get(i);
            if (b.owner.equals(owner))
                System.out.println(b.owner + " " + b.balance);
        }
        return null;
    }

    public BankAccount getAllAccount() {
        int sortat;
        do {
            sortat = 1;
            for (int i = 0; i < bankAccounts.size() - 1; i++) {
                BankAccount b1 = (BankAccount) bankAccounts.get(i);
                BankAccount b2 = (BankAccount) bankAccounts.get(i + 1);
                BankAccount b3 = new BankAccount(" ", 0);

                if (b1.owner.compareTo(b2.owner) > 0) {
                    b3.owner = b1.owner;
                    b3.balance = b1.balance;
                    b1.owner = b2.owner;
                    b1.balance = b2.balance;
                    b2.owner = b3.owner;
                    b2.balance = b3.balance;
                    sortat = 0;
                }
            }
        } while (sortat == 0);

        for (int i = 0; i < bankAccounts.size(); i++) {
            BankAccount b = (BankAccount) bankAccounts.get(i);
            System.out.println(b.owner + " " + b.balance);
        }
        return null;
    }

}
