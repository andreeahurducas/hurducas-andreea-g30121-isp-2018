package andreea.hurducas.lab6.ex2;


public class Test {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Andreea", 1000);
        bank.addAccount("Monica", 5500);
        bank.addAccount("Laura", 2500);
        bank.addAccount("Mihai", 1020);


        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between limits");
        bank.printAccounts(0, 1500);
        System.out.println("Get Account by owner name");
        bank.getAccount("Monica");
        System.out.println("Get All Account order by name");
        bank.getAllAccount();

    }

}
