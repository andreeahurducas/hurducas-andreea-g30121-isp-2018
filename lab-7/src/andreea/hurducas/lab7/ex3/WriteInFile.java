package andreea.hurducas.lab7.ex3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class WriteInFile {
    public void write() throws Exception {
        try {
            File myFile = new File("data2.txt");
            PrintWriter scriere = new PrintWriter("data2.txt");
            Scanner citire = new Scanner(System.in);
            System.out.println("Write the text you want to encrypt ");
            String l = citire.nextLine();
            scriere.print(l);
            scriere.close();
        } catch (FileNotFoundException e) {
            System.out.println("Invalid");
        }
    }
}
