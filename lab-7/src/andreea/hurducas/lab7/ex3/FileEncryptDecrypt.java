package andreea.hurducas.lab7.ex3;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileEncryptDecrypt {
    public File file;
    public PrintWriter print;

    public void encrypt() throws Exception {
        try {
            WriteInFile a = new WriteInFile();
            a.write();
            file = new File("data2.txt");
            Scanner citirefis = new Scanner(file);
            print = new PrintWriter("data.enc");
            String l = citirefis.nextLine();
            char sir = ' ';
            for (int i = 0; i < l.length(); i++) {
                sir = (char) (l.charAt(i) << 1);
                print.print(sir);

            }
            print.close();
        } catch (Exception e) {
            System.out.println("Error");
        }
    }

    public void decrypt() {
        try {
            file = new File("data.enc");
            print = new PrintWriter("data.dec");
            Scanner s1 = new Scanner(file);
            System.out.println("");
            int i = 0;
            char sir2 = ' ';
            String l1 = s1.next();
            while (i < l1.length()) {
                sir2 = (char) (l1.charAt(i) >> 1);
                i++;
                print.print(sir2);
            }
            print.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
