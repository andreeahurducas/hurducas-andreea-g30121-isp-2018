package andreea.hurducas.lab7.ex4;

import java.io.Serializable;
import java.util.Scanner;

public class Car implements Serializable {
    public String model;
    public int price;

    public Car() {
    }

    public Car(String model, int price) {
        this.model = model;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public int getPrice() {

        return price;
    }

    public void setModel(String model) {

        this.model = model;
    }

    public void setPrice(int price) {

        this.price = price;
    }

    @Override
    public String toString() {
        return "Car type: " + this.model + "\nPrice: " + this.price;
    }

    public void add_Car() {

        Scanner citire = new Scanner(System.in);
        System.out.println("Give the car model: ");
        model = citire.nextLine();
        System.out.println("Give the price of the car: ");
        price = citire.nextInt();
        Car myCar = new Car(model, price);
    }

    public String show_Car() {
        return "Car type: " + this.model + "\nPrice: " + this.price;
    }
}
