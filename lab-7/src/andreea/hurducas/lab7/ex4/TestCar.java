package andreea.hurducas.lab7.ex4;

import java.io.*;
import java.util.Scanner;

public class TestCar {
    public static void main (String[] args) throws Exception {
        String filename;
        Car a = new Car ();
        Scanner citire = new Scanner(System.in);
        int opt = -1;
        while (opt != 6) {
            System.out.println ("\n1)Add_car\n2)Show_car\n3)Save_Car_Objects\n4)View_Existing_Car_Objects\n5)Read_and_Display\n6)Exit\n");
            System.out.println ("Choose an option: ");
            opt = citire.nextInt ();
            switch (opt) {
                case 1:
                    a.add_Car ();
                    break;
                case 2:
                    System.out.println (a.show_Car ());
                    break;
                case 3:
                    FileOutputStream fos = null;
                    ObjectOutputStream out = null;
                    try {
                        System.out.println ("Save with the next name(.ser) :");
                        filename = citire.next ();
                        if (filename.endsWith (".ser")) {
                            fos = new FileOutputStream (filename);
                            out = new ObjectOutputStream (fos);
                            out.writeObject (a);
                            out.close ();
                        }
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                    break;
                case 4:
                    File folder = new File ("C:\\Users\\wfc_adm\\Desktop\\Andreea Hurducas\\lab-7\\src\\andreea\\hurducas\\lab7\\ex4");
                    File[] listOfFiles = folder.listFiles ();
                    for (File file : listOfFiles) {
                        if (file.getName ().endsWith (".ser")) {
                            System.out.print (" " + file.getName () + "   ");
                        }
                    }
                    break;
                case 5:
                    FileInputStream fis = null;
                    ObjectInputStream in = null;
                    try {
                        System.out.println ("Get the next filename(.ser): ");
                        filename = citire.next ();
                        if (filename.endsWith (".ser")) {
                            fis = new FileInputStream (filename);
                            in = new ObjectInputStream (fis);
                            a = (Car) in.readObject ();
                            in.close ();
                            System.out.println (a);
                        }
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                    break;
                case 6:
                    break;
                default:
                    System.out.println ("Invalid");
            }
        }

    }
}
