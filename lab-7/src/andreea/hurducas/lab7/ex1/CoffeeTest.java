package andreea.hurducas.lab7.ex1;

import java.util.Scanner;

public class CoffeeTest {
    public static void main(String[] args) {
        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();
        /*Scanner s = new Scanner(System.in);
        System.out.println("NO= ");
        int no = s.nextInt();
        */
        for (int i = 0; i < 15; i++) {
            Coffee c = mk.makeCoffee();
            try {

                d.drinkCoffee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception:" + e.getMessage() + " temp=" + e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc());
            } catch (NumberException e) {
                System.out.println("Exception:" + e.getMessage() + "predefined number of coffees=" + e.getPred());
            } finally {
                System.out.println("Throw the coffee cup.\n");
            }
        }
    }
}
